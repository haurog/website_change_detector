# timing tracker adapted from https://blog.sicara.com/profile-surgical-time-tracking-python-db1e0a5c06b6

from time import time
from functools import wraps


def simple_time_tracker(logging):  # logging is the python logger
    def _simple_time_tracker(fn):
        @wraps(fn)
        def wrapped_fn(*args, **kwargs):
            start_time = time()

            try:
                result = fn(*args, **kwargs)
            finally:
                elapsed_time = time() - start_time
                logging.info(f'SimpleTimeTracker: {fn.__name__}({args}) execution time: {elapsed_time:.3f}')
            return result
        return wrapped_fn
    return _simple_time_tracker