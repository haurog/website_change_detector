import requests
import os
import difflib
import textwrap
import logging
import re
from bs4 import BeautifulSoup
import yaml
from requests.adapters import HTTPAdapter

# from simple_time_tracker import simple_time_tracker


# @simple_time_tracker(logging)
def load_pages_file(pages=''):
    # loades a yaml encoded file with websites from the disk
    filename = 'pagestocheckdummy.txt'
    if pages in os.listdir():
        filename = pages
    else:
        logging.error(f'Filename \"{pages}\" not found in current working directory try fallback file.')
        if filename not in os.listdir():
            logging.error(f'Fallback file \"{filename}\" not found in current working directory return empty list.')
    with open(filename, 'r') as f:
        websites = yaml.load(f)
        # websites = [line for line in f if line.rstrip() and line[0] != '#']
    logging.debug(f'File \"{filename}\" loaded.')
    logging.debug(websites)
    return websites


# @simple_time_tracker(logging)
def get_website_content(website):
    header = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/71.0.3578.80 Chrome/71.0.3578.80 Safari/537.36'}
    session = requests.Session()
    adapter = HTTPAdapter(max_retries=5)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    try:
        logging.debug(f'Website: {website["url"]}')
        req = session.get(website['url'], headers=header)
        req.raise_for_status()
        logging.info(f'Connection to website {website["url"]} successful ')
        logging.debug(f'Website content: {req.text[:100]}')
        soup = BeautifulSoup(req.text, 'html.parser')
        website_text = ''
        if 'bs_find' in website.keys():
            for element in website['bs_find']:
                if 'attr' in element.keys():
                    temp_soup = soup.find(element['tag'], attrs={element['attr']: element['value']})
                    if temp_soup:
                        website_text += temp_soup.text
                        # website_text += temp_soup.prettify()
                else:
                    temp_soup_list = soup.find(element['tag'])
                    if temp_soup:
                        website_text += temp_soup.text
        else:
            website_text = soup.text

        website_text = re.sub(r"\n+", "\n", website_text)  # Remove empty lines
        website_text = re.sub(r"\n +", "\n", website_text)  # Remove spaces at start of a line
        website_text = re.sub(r"\n\t+", "\n", website_text)  # Remove tabs at start of a line

        logging.debug(f'Website content after soup: {website_text[:]}')
        return [website, website_text]
    except Exception as exc:
        logging.error(f'Could not connect to and parse website: {website["url"]}. Exception: {exc}')
        return [website, f'Could not connect to and parse website: {website["url"]}. Exception: {exc}']


def make_diff(stored_content, new_content):

    added = []
    removed = []
    max_length = 77  # Later, additional whitespaces will be added to each line for indentation

    # Only compare part of a line (first 1000 characters). Otherwise it might become very slow.
    stored_pruned = [line[:1000] for line in stored_content.splitlines()]
    new_pruned = [line[:1000] for line in new_content.splitlines()]

    differ_class = difflib.Differ()
    diff = list(differ_class.compare(stored_pruned, new_pruned))

    for element in diff:
        if element[2:].strip():
            ending = ''
            if len(element[2:]) > max_length:
                ending = '...'
            if element.startswith("+"):
                added.append(element[2:max_length+2] + ending)
            elif element.startswith("-"):
                removed.append(element[2:max_length+2] + ending)

    diff_string = ''
    if added and len(added) < 20:
        diff_string = "\n Added Lines:\n  " + '\n  '.join(added)
    elif added:
        diff_string = f"\n Added Lines:\n  Too many to list them all: {len(added)} lines"
    if removed and len(removed) < 20:
        diff_string = diff_string + "\n Removed Lines:\n  " + '\n  '.join(removed)
    elif removed:
        diff_string = diff_string + f"\n Removed Lines:\n  Too many to list them all: {len(removed)} lines"

    return diff_string


# @simple_time_tracker(logging)
def analyse_website_results(website_results, changed_websites, websites_storage):
    for result in website_results:
        website, content = result

        if not content:
            changed_websites.append(f'No content from \"{website["url"]}\".')
        elif "Could not connect to and parse website" in content:
            changed_websites.append(content)
        elif website['url'] in websites_storage.keys():
            if content != websites_storage[website['url']]:
                logging.error(f'Content has changed for {website["url"]}')

                with open(f'difftestwebsite.txt', 'w') as outfile:
                    outfile.write(content)
                with open(f'difftestwebsitestored.txt', 'w') as outfile:
                    outfile.write(websites_storage[website['url']])

                diff = make_diff(websites_storage[website['url']], content)

                websites_storage[website['url']] = content
                emailstring = ''
                if 'name' in website.keys():
                    emailstring = website['name'] + ': '
                emailstring += website['url']
                emailstring += diff
                changed_websites.append(emailstring)

            else:
                logging.info(f'Website \"{website["url"]}\" is unchanged.')
        else:
            websites_storage[website['url']] = content
    return changed_websites
