#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `website_change_detector` package."""

import pytest

import website_change_detector.website_functions as wf
import website_change_detector.email_functionality as ef


def test_pagestocheck_is_properly_formatted():
    # cheap linter for the yaml files
    import yaml

    filename = 'pagestocheck.txt'
    with open(filename, 'r') as f:
        websites = yaml.load(f)

    filename = 'pagestocheckdummy.txt'
    with open(filename, 'r') as f:
        websites = yaml.load(f)

    assert websites


def test_load_pages_normal():

    res = wf.load_pages_file('../pagestocheckdummy.txt')

    assert res


# @pytest.fixture
# def response():
#     """Sample pytest fixture.

#     See more at: http://doc.pytest.org/en/latest/fixture.html
#     """
#     # import requests
#     # return requests.get('https://github.com/audreyr/cookiecutter-pypackage')


# def test_content(response):
#     """Sample pytest test function with the pytest fixture as an argument."""
#     # from bs4 import BeautifulSoup
#     # assert 'GitHub' in BeautifulSoup(response.content).title.string


# def test_command_line_interface():
#     """Test the CLI."""
#     runner = CliRunner()
#     result = runner.invoke(cli.main)
#     assert result.exit_code == 0
#     assert 'website_change_detection.cli.main' in result.output
#     help_result = runner.invoke(cli.main, ['--help'])
#     assert help_result.exit_code == 0
#     assert '--help  Show this message and exit.' in help_result.output
