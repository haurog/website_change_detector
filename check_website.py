# This program downloads all the websites (html only) supplied, searches for the relevant parts
# (user supplied) and stores it. It compares the downloaded website with an already stored version
# and sends an email if something has changed


import website_functions as wf
import email_functionality as ef
import logging
import logging.handlers
import shelve
import os
import sys
import yaml
import argparse
import time
import concurrent.futures


start_time = time.time()
if __name__ == "__main__":
    os.chdir(sys.path[0])

    parser = argparse.ArgumentParser(description="This program parses all urls in a given file " +
                                                 "and downloads the html for comparison. It will " +
                                                 "send an email if the website has changed.")
    parser.add_argument('--url_file', type=str,
                        help="A YAML formatted file tih the urls and additional " +
                             "search information.")
    parser.add_argument('--send_email', action='store_true',
                        help="If this arg is supplied the program will send an email to the " +
                             "supplied address in the email_credentials.txt.")
    parser.add_argument('--write_logs', action='store_true',
                        help="If this arg is supplied the program will write log files into the"
                             "folder 'logs'.")
    args = parser.parse_args()

    # config logging
    if not os.path.exists('logs'):
        os.makedirs('logs')

    handlers = [logging.StreamHandler()]
    if args.write_logs:
        handlers.append(logging.handlers.TimedRotatingFileHandler("logs/check_websites.log",
                                                                  when='midnight',
                                                                  interval=1,
                                                                  backupCount=10))
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(levelname)s - %(message)s',
                        handlers=handlers)

    logging.debug('start')

    url_file = ''
    if args.url_file:
        url_file = args.url_file

    websites = wf.load_pages_file(url_file)

    # with concurrent.futures.ThreadPoolExecutor(1) as pool:
    with concurrent.futures.ThreadPoolExecutor(len(websites['websites'])) as pool:
        website_results = pool.map(wf.get_website_content, websites['websites'])

    with shelve.open('websites_storage.bin', writeback=True) as websites_storage:
        changed_websites = []
        changed_websites = wf.analyse_website_results(website_results, changed_websites, websites_storage)

    if args.send_email:
        try:
            ef.send_email(changed_websites)
        except Exception as exc:
            error_message = f'Exception while trying to send email. Exception: {exc}'
            logging.error(error_message)
            ef.send_email([error_message])
    if changed_websites:
        message = '\n\nThe following pages have changed:\n\n'
        temp = '\n\n'.join(changed_websites)
        message += temp
        logging.info(message)
    else:
        message = "\n\nNo changes in the monitored websites"
        logging.info(message)
    elapsed_time = time.time() - start_time
    print("\n")
    logging.info(f'execution time: {elapsed_time}')
