import smtplib
import time
import email
from email.mime.text import MIMEText


def send_email(message_list=[]):
    filename = 'email_credentials.txt'

    with open(filename, 'r') as f:
        email_credentials = dict((line.strip().split(' = ') for line in f))
    smtp_object = smtplib.SMTP(email_credentials['server'], email_credentials['port'])
    smtp_object.ehlo()
    smtp_object.starttls()
    smtp_object.login(email_credentials['login_name'], email_credentials['password'])

    msg = email.mime.text.MIMEText("")
    if message_list:
        message = "The following pages have changed:\n\n"
        message += '\n\n'.join(message_list)
        msg = email.mime.text.MIMEText(message)
        msg['Subject'] = email.header.Header("Webscraper Status: changes detected")

    else:
        msg['Subject'] = email.header.Header("Webscraper Status: no changes")

    # msg = email.mime.text.MIMEText("")
    msg['From'] = email_credentials['from_mail']
    msg['To'] = email_credentials['to_mail']
    utc_from_epoch = time.time()
    msg['Date'] = email.utils.formatdate(utc_from_epoch, localtime=True)

    smtp_object.sendmail(email_credentials['from_mail'],
                         email_credentials['to_mail'],
                         msg=msg.as_string())
    smtp_object.quit()

if __name__ == "__main__":
    dummy_list = ['sdfgüdf', 'sdfdäägdf', 'sdfg$df']
    send_email(dummy_list)
